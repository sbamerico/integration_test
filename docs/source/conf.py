#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sphinx_rtd_theme

html_theme = "sphinx_rtd_theme"

html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

def setup(app):
    app.add_stylesheet("main_stylesheet.css")

extensions = ['breathe', 'sphinx.ext.intersphinx', 'sphinx.ext.autosectionlabel', 'sphinx.ext.autodoc']

intersphinx_mapping = {
   'docdev0': ('http://localhost:8000/docs/test-repo/en/2.0/', None),
   'exhale':  ('https://exhale.readthedocs.io/en/latest/', None)
}

templates_path = ['_templates']
html_static_path = ['_static']
source_suffix = '.rst'
master_doc = 'index'
project = 'devdoc'

exclude_patterns = []
highlight_language = 'cpp'
primary_domain = 'cpp'
pygments_style = 'sphinx'
todo_include_todos = False
htmlhelp_basename = 'devdoc'

